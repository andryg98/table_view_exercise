import UIKit

class HomeViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel: HomeViewModel = HomeViewModel(items: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 90.0
    }
    
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = viewModel.getItem(for: indexPath) else { return UITableViewCell() }
        switch cellType {
        case .textField:
            let cell = tableView.dequeueReusableCell(withIdentifier: "textFieldCell", for: indexPath) as! TextFieldTableViewCell
            cell.textField.delegate = self
            return cell
        case .label:
            let cell = tableView.dequeueReusableCell(withIdentifier: "labelCell", for: indexPath) as! LabelTableViewCell
            cell.setText(viewModel.userInputText)
            return cell
        case .background:
            let cell = tableView.dequeueReusableCell(withIdentifier: "backgroundCell", for: indexPath) as! BackgroundTableViewCell
            cell.setBackgroundColor(viewModel.userInputText)
            return cell
        case .slider:
            let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! SliderTableViewCell
            cell.delegate = self
            cell.setupSlider(with: Float(viewModel.numberOfItems))
            cell.slider.isUserInteractionEnabled = viewModel.sliderUserInteractionEnabled
            return cell
        case .switch:
            let cell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath) as! SwitchTableViewCell
            cell.delegate = self
            return cell
        }
    }
    
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        viewModel.userInputText = textField.text ?? ""
        self.tableView.reloadRows(at: viewModel.getIndexPathsFor([.background, .label]), with: .none)
    }
}

extension HomeViewController: SliderTableViewCellDelegate {
    func numberOfRowsDidChange(_ numberOfRows: Int) {
        viewModel.changeNumberOfRows(to: numberOfRows)
        self.tableView.reloadData()
    }
}

extension HomeViewController: SwitchTableViewCellDelegate {
    func setSliderStatus(_ isActive: Bool) {
        viewModel.sliderUserInteractionEnabled = isActive
        self.tableView.reloadRows(at: viewModel.getIndexPathsFor([.slider]), with: .none)
    }
}
