import UIKit

enum CellType {
    case textField, label, background, slider, `switch`
}


enum Color: String {
    
    case red, blue, green, orange, white, black
    
    var uiColor: UIColor {
        switch self {
        case .red:
            return UIColor.red
        case .blue:
            return UIColor.blue
        case .green:
            return UIColor.green
        case .orange:
            return UIColor.orange
        case .white:
            return UIColor.white
        case .black:
            return UIColor.black
        }
    }
    
}
