import UIKit

protocol SwitchTableViewCellDelegate {
    func setSliderStatus(_ isActive: Bool)
}

class SwitchTableViewCell: UITableViewCell {
    
    static var type = CellType.switch
    
    @IBOutlet weak var sliderToggle: UISwitch!
    
    public var delegate: SwitchTableViewCellDelegate?
    
    @IBAction func switchValueDidChange(_ sender: UISwitch) {
        delegate?.setSliderStatus(sender.isOn)
    }
}
