import UIKit

protocol SliderTableViewCellDelegate {
    func numberOfRowsDidChange(_ numberOfRows: Int)
}

class SliderTableViewCell: UITableViewCell {
    
    static var type = CellType.slider
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var sliderLabel: UILabel!
    
    var delegate: SliderTableViewCellDelegate?
    
    func setupSlider(with maximumValue: Float) {
        slider.minimumValue = 0.0
        slider.maximumValue = maximumValue
        slider.setNeedsLayout()
    }
    
    @IBAction func numberOfCellsChange(_ sender: UISlider) {
        let step: Float = 1
        let currentValue = round((sender.value - sender.minimumValue) / step)
        slider.value = currentValue
        sliderLabel.text = "Number of rows: \(Int(currentValue))"
        delegate?.numberOfRowsDidChange(Int(currentValue))
    }
    
}
