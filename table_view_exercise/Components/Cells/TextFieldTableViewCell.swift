import UIKit

class TextFieldTableViewCell: UITableViewCell {

    static var type = CellType.textField
    
    @IBOutlet weak var textField: UITextField!

}
