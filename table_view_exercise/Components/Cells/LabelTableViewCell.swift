import UIKit

class LabelTableViewCell: UITableViewCell {
    
    static var type = CellType.label
    
    @IBOutlet weak var textLbl: UILabel!
    
    func setText(_ text: String) {
        textLbl.text = text
    }

}
