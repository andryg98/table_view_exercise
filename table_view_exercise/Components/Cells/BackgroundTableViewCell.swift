import UIKit

class BackgroundTableViewCell: UITableViewCell {
    
    static var type = CellType.background
    
    func setBackgroundColor(_ colorString: String) {
        guard let color = Color(rawValue: colorString) else {
            backgroundColor = .lightGray
            return
        }
        backgroundColor = color.uiColor
    }
    
}
