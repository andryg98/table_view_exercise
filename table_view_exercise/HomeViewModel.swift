import Foundation

class HomeViewModel {
    
    private let itemsPool: [CellType] = [.slider, .textField, .label, .background, .switch]
    
    public var items: [CellType] = []
    public var userInputText: String = ""
    public var sliderUserInteractionEnabled: Bool = true
    
    lazy public var numberOfItems: Int = {
        return itemsPool.filter({ $0 != .slider}).count
    }()
    
    init(items: [CellType]?) {
        self.setItems(items)
    }
    
    public func getIndexPathsFor(_ types: [CellType]) -> [IndexPath] {
        return items.indices.filter { types.contains(self.items[$0]) }.map { IndexPath(row: $0, section: 0) }
    }
    
    public func changeNumberOfRows(to numOfRows: Int) {
        var newItems: [CellType] = [.slider]
        newItems.append(contentsOf: itemsPool.filter({ $0 != .slider })[randomPick: numOfRows])
        self.items = newItems
    }
    
    public func setItems(_ items: [CellType]?) {
        self.items = items ?? itemsPool
    }
    
    public func getItem(for indexPath: IndexPath) -> CellType? {
        return items[indexPath.row]
    }
    
}
